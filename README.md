# filter-lists

filters for e.g., [uBlock Origin](https://github.com/gorhill/uBlock) (recommended) or Adblock Plus

## How to Use with uBlock Origin

1. Click on the uBlock Origin icon.
2. Click the settings/"Open the dashboard" option.
3. Go to the "Filter lists" tab.
4. Scroll down to the bottom and check "Import..."
5. Paste the URL for the list you would like to add.
6. Click "Apply changes".

## Filters

Note: These filters are not considered comprehensive lists that will block everything. They are a best-effort attempt. These lists come with no warranty whatsoever and **are likely to break things**. Please use discretion when applying them.

### Anti-Amazon

Blocks Amazon-owned domains. This will break a lot of the web because it blocks AWS.

URL: https://gitlab.com/vecna4/filter-lists/raw/master/lists/anti-amazon.txt

### Anti-Bezos

Blocks Jeff Bezos-owned domains. This list should automatically import the Anti-Amazon list as well, if you use uBlock Origin. The most notable non-Amazon domain blocked by this list is The Washington Post.

URL: https://gitlab.com/vecna4/filter-lists/raw/master/lists/anti-bezos.txt

### Anti-Facebook

Blocks Facebook-owned domains. Theoretically shouldn't break anything besides Facebook/Messenger/Instagram/WhatsApp and Facebook-/Instagram-sign-ons, but I'm not sure.

URL: https://gitlab.com/vecna4/filter-lists/raw/master/lists/anti-facebook.txt

### Anti-Google

Blocks Google-owned domains. This will break a lot of the web.

URL: https://gitlab.com/vecna4/filter-lists/raw/master/lists/anti-google.txt

## Contributing

If you can improve any of these lists, please either send me an email (vecna@disroot.org) or submit a pull request.

## License

Public domain/CC0
