! Title: Anti-Amazon Filter List
! Description: Filter list for e.g., uBlock Origin or AdBlock Plus to
!              block Amazon domains. As Amazon owns a lot of stuff,
!              this list will likely break a lot of sites.
! Expires: 4 days
! Licence: https://gitlab.com/vecna4/filter-lists/LICENSE
!          (Public Domain/CC0)
! Homepage: https://gitlab.com/vecna4/filter-lists
!
! THIS LIST IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
! EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
! MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
! IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
! OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
! ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
! OTHER DEALINGS IN THE SOFTWARE.
!
! Pull requests welcome.
!
! Link to this file: https://gitlab.com/vecna4/filter-lists/raw/master/lists/anti-amazon.txt

! Amazon.com, localizations, link shortening
||amazon.*^
||amzn.com^
||amzn.to^

! Amazon AWS (powers a lot of the web)
||*.aws^
||amazonaws.*^
||awsstatic.com^
||cloudfront.net^

! Other domains (Amazon products, etc.)
||a9.com^
||abebooks.com^
||abebookscdn.com^
||alexa.com^
||alexametrics.com^
||amazon-adsystem.com^
||amazongames.com^
||audible.com^
||brilliancepublishing.com^
||comixology.com^
||createspace.com^
||dpreview.com^
||goodreads.com^
||gr-assets.com^
||imdb.com^
||media-amazon.com^
||media-imdb.com^
||primevideo.com^
||ssl-images-amazon.com^
||twitch.tv^
||twitchcdn.net^
||twitchsvc.net^
||woot.com^
||zappos.com^
