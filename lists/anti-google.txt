! Title: Anti-Google Filter List
! Description: Filter list for e.g., uBlock Origin or AdBlock Plus to
!              block Google domains. As Google owns a lot of stuff,
!              and a lot of it is actually useful, this will likely
!              break a lot of sites.
! Expires: 4 days
! Licence: https://gitlab.com/vecna4/filter-lists/LICENSE
!          (Public Domain/CC0)
! Homepage: https://gitlab.com/vecna4/filter-lists
!
! THIS LIST IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
! EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
! MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
! IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
! OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
! ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
! OTHER DEALINGS IN THE SOFTWARE.
!
! Pull requests welcome.
!
! Link to this file: https://gitlab.com/vecna4/filter-lists/raw/master/lists/anti-google.txt

! Google.com and localizations
||google.*^
||google.co.*^
||google.com.*^

! Google TLD
||*.google^

! Assorted Google shortening and redirecting
||g.co^
||ggoogle.com^
||gogle.com^
||gogole.com^
||goo.gl^
||googel.com^
||googil.com^
||googl.com^
||googlee.com^
||googlr.com^
||goolge.com^
||gooogle.com^

! Other domains (Google products, etc.)
||1e100.net^
||466453.com^
||admob.com^
||adsense.com^
||adwords.com^
||abc.xyz^
||android.com^
||appspot.com^
||blogger.*^
||blogspot.*^
||chromium.org^
||chrome.co^
||chrome.com^
||chromebook.com^
||cobrasearch.com^
||constituteproject.org^
||creativelab5.com^
||doubleclick.com^
||feedburner.com^
||flutter.dev^
||flutter.io^
||foofle.com^
||froogle.com^
||ggpht.com^
||google-analytics.com^
||googleadservices.com^
||googleanalytics.com^
||googleapps.com^
||googleapis.com^
||googlearth.com^
||googleblog.com^
||googlebot.com^
||googlecapital.com^
||googlecode.com^
||googlecommerce.com^
||googledrive.com^
||googleearth.com^
||googlelabs.com^
||googlemail.com^
||googlemaps.com^
||googlepagecreator.com^
||googlescholar.com^
||googlesource.com^
||googlestore.com^
||googlesyndication.com^
||googletagmanager.com^
||googleusercontent.com^
||googleventures.com^
||gmail.com^
||gmodules.com^
||grasshopper.codes^
||gv.com^
||gwtproject.org^
||gybo.com^
||igoogle.com^
||keyhole.com^
||like.com^
||lively.com^
||madewithcode.com^
||nest.com^
||orkut.com^
||panoramio.com^
||picasa.com^
||schemer.com^
||solveforx.com^
||textcube.com^
||thinkwithgoogle.com^
||tiltbrush.com^
||urchin.com^
||virustotal.com^
||waze.com^
||wdyl.com^
||webpagetest.org^
||webrtc.org^
||whatbrowser.org^
||wildfireapp.com^
||withgoogle.com^
||x.company^
||yourprimer.com^
||youtu.be^
||youtube.com^
||youtube-nocookie.com^
||youtubeeducation.com^
||youtubegaming.com^
||yt.be^
||ytimg.com^
||zygotebody.com^

! Notes
! orkut.com's successor hello.com is not a Google product.
! zagat.com is no longer owned by Google.
